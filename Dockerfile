FROM java:8
VOLUME /tmp
#ARG JAR_FILE=target/surefire-reports/*.jar
#COPY ${JAR_FILE} app.jar
#EXPOSE 8080
#COPY target/surefire-reports/spring-boot-unit-integration-test-example-0.0.1-SNAPSHOT.jar spring-boot-unit-integration-test-example.jar
#COPY opt/atlassian/pipelines/agent/build/target/*.jar /usr/app/
#WORKDIR /usr/app/
#RUN sh -c 'touch *.jar'
#ADD opt/atlassian/pipelines/agent/build/target/*.jar app.jar
#CMD ["java","-jar","app.jar"]
#ENTRYPOINT ["java" "-jar" "*.jar"]
ARG JAR_FILE=/opt/atlassian/pipelines/agent/build/target/*.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar"]